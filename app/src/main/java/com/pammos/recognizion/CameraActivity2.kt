package com.pammos.recognizion

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.SurfaceView
import android.view.View
import android.widget.Toast
import bolts.Task.delay
import kotlinx.android.synthetic.main.custom_camera.*
import android.content.Context
import android.graphics.*
import android.hardware.*
import android.hardware.Camera
import android.hardware.SensorManager
import android.os.Environment
import com.pammos.recognizion.extensions.*
import org.opencv.core.CvType
import org.opencv.core.Mat
import org.opencv.core.Size
import org.opencv.imgproc.Imgproc
import java.io.*
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList


data class Measurment(var Sys: Int, var Dia: Int, var pulse: Int): Serializable
data class OxyMeasurment(var oximeter: Int, var pulse: Int): Serializable
data class Glucometer(var value: Int): Serializable

class CameraActivity2 : AppCompatActivity(), Camera.PreviewCallback, SensorEventListener{



    lateinit var camera: Camera
    lateinit var showCamera: ShowCamera
    var listMeasurment = ArrayList<Measurment>()
    var listImages = ArrayList<Bitmap>()

    var isProcessing = false
    var axisY: Float = 90.0F

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_camera)

        camera = Camera.open()
        showCamera = ShowCamera(this, camera)
        frame_camera.addView(showCamera)
        camera.setPreviewCallback(this)

        val sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager

        val rotationVectorSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR)

        sensorManager.registerListener(this, rotationVectorSensor, SensorManager.SENSOR_DELAY_NORMAL)

    }

    fun capture(){
        if (camera != null){

        }
    }

    fun captureImage(view: View){
        isProcessing = true

    }

    override fun onBackPressed() {
        super.onBackPressed()
        camera.stopPreview()
        val returnIntent = Intent()
        setResult(2, returnIntent)
        finish()
    }

    override fun onPause() {
        super.onPause()
        //camera.release()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()


    }


    override fun onPreviewFrame(data: ByteArray?, camera: Camera?) {

        try {

            val parameters = camera!!.getParameters()
            val width = parameters.previewSize.width
            val height = parameters.previewSize.height

            val mRgba = Mat()
            val mYuv = Mat(height + height / 2, width, CvType.CV_8UC1)

            mYuv.put(0, 0, data)
            Imgproc.cvtColor(mYuv, mRgba, Imgproc.COLOR_YUV420sp2RGB, 4)
            Imgproc.resize(mRgba, mRgba, Size(400.0, 250.0))
            val imageBitmap = mRgba.toBitmap()

            if (imageBitmap != null ) {

                isProcessing = false

                val matrix = Matrix()
                if (imageBitmap.width > imageBitmap.height) {
                    matrix.postRotate(90F)
                }
                var bitmap = Bitmap.createBitmap(imageBitmap, 0, 0, imageBitmap.width, imageBitmap.height, matrix, false)

                /*var listImages = ArrayList<Bitmap>()

                for (i in 1 until 16){
                    var bitmap = getBitmapFromAssets("$i")
                    val sejoy = getValueSejoy(bitmap)

                    Log.e("Brillo Imagen", "$i ${sejoy.second.Sys}")
                    val imageBitmap = BitmapFactory.decodeByteArray(sejoy.first, 0, sejoy.first.size)
                    listImages.add(imageBitmap)
                }*/

                //

                /*val tensiometer = getValueSejoy(bitmap)

                val measure = tensiometer.second*/

                val glucometer = getValueGlucometerVivaChek(bitmap)
                val measure = glucometer.second

                /*val imageArray = glucometer.first
                camera.stopPreview()
                val returnIntent = Intent()
                returnIntent.putExtra("result", imageArray)
                returnIntent.putExtra("value", measure)
                setResult(1, returnIntent)
                finish()*/

                Log.e("Capturando", "Validando imagen")
                if (verifyValuesGlucometer(measure)) {

                    val imageArray = glucometer.first
                    camera.stopPreview()
                    val returnIntent = Intent()
                    returnIntent.putExtra("result", imageArray)
                    returnIntent.putExtra("value", measure)
                    setResult(1, returnIntent)
                    finish()
                }
            }else{
                Log.e("Capturando", "Bitmap Nulo")
            }
        }catch (e: Exception){
            e.printStackTrace()
        }

    }

    private fun verifyValues(measure: Measurment): Boolean{
        Log.e("Valores", "${measure.Sys}, ${measure.Dia}, ${measure.pulse}")
        return when {
            measure.Sys < 40  || measure.Sys > 290-> false
            measure.Dia < 30 || measure.Dia > 200 -> false
            measure.pulse < 30 || measure.pulse > 200-> false
            else -> true
        }
    }

    private fun verifyValuesOximeter(measure: OxyMeasurment): Boolean{
        Log.e("Valores", "${measure.oximeter}, ${measure.pulse}")
        return when {
            measure.oximeter < 80  || measure.oximeter > 100-> false
            measure.pulse < 40 || measure.pulse > 200-> false
            else -> true
        }
    }

    private fun verifyValuesGlucometer(measure: Glucometer): Boolean{
        Log.e("Valores", "${measure.value}")
        return when {
            measure.value < 40  || measure.value > 390-> false
            else -> true
        }
    }

    private fun valueCorrect(): Boolean{
        if (listMeasurment.size >= 2) {
            for (i in 1 until listMeasurment.size) {
                for (j in i + 1 until listMeasurment.size) {
                    if (listMeasurment[i] == listMeasurment[j]) {
                        return true
                    }
                }
            }
        }
        if (listMeasurment.size >= 10){
            listMeasurment.removeAt(0)
            listImages.removeAt(0)
        }
        return false
    }

    private fun saveImages(){
        for (i in 0 until listImages.size){
            saveImage(listImages[i], i)
        }
    }

    private fun getBitmapFromAssets(fileName: String): Bitmap {
        val assetManager = assets
        var inputStream: InputStream? = null
        try {
            inputStream = assetManager.open("images/$fileName.jpeg")
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return BitmapFactory.decodeStream(inputStream)
    }


    fun saveImage(bmp: Bitmap, pos: Int){
        val file_path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/AReconog"
        val dir = File(file_path);
        if(!dir.exists()) {
            dir.mkdirs()
        }

        val date = Date()

        val formatter = SimpleDateFormat("yyMMddHHmmssSSS")
        val strDate = formatter.format(date)

        val file = File(dir, "$strDate.jpeg")
        val fOut = FileOutputStream(file)

        bmp.compress(Bitmap.CompressFormat.JPEG, 85, fOut)
        fOut.flush()
        fOut.close()

    }


    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {

    }

    override fun onSensorChanged(event: SensorEvent?) {
        val rotationMatrix = FloatArray(16)
        SensorManager.getRotationMatrixFromVector(rotationMatrix, event!!.values)

        // Remap coordinate system
        val remappedRotationMatrix = FloatArray(16)
        SensorManager.remapCoordinateSystem(rotationMatrix,
                SensorManager.AXIS_X,
                SensorManager.AXIS_Z,
                remappedRotationMatrix)

        // Convert to orientations
        val orientations = FloatArray(3)
        SensorManager.getOrientation(remappedRotationMatrix, orientations)



        for (i in 0..2) {
            orientations[i] = Math.toDegrees(orientations[i].toDouble()).toFloat()
            if (i == 1){
                axisY = orientations[i]
            }

            //Log.e("Sensores", "Orientacion: " + i + " Valor: "  + orientations[i])
        }
    }

}
