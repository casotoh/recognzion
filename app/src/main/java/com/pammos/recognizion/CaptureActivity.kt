package com.pammos.recognizion

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.graphics.*
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import kotlinx.android.synthetic.main.activity_capture.*
import org.opencv.android.Utils

import org.opencv.core.Mat
import org.opencv.core.Point
import org.opencv.core.Size
import org.opencv.imgproc.Imgproc
import org.opencv.core.CvException
import android.graphics.Bitmap
import android.os.Environment
import com.pammos.recognizion.extensions.*
import org.opencv.android.InstallCallbackInterface
import org.opencv.android.LoaderCallbackInterface
import org.opencv.android.OpenCVLoader
import org.opencv.android.OpenCVLoader.initAsync
import org.opencv.core.Scalar
import org.opencv.core.CvType
import java.io.File
import java.io.FileOutputStream


class CaptureActivity : AppCompatActivity() {

    private val TAKE_PHOTO_REQUEST = 101
    val REQUEST_IMAGE_CAPTURE = 1
    private val CAMERA_REQUEST_CODE = 12345
    private val REQUEST_GALLERY_CAMERA = 54654
    private var mCurrentPhotoPath: String = ""
    val TAG = "CaptureActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_capture)
        floatingCapture?.setOnClickListener { validatePermissions() }
    }



    private fun validatePermissions() {
        Dexter.withActivity(this)
            .withPermissions(Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)

            .withListener(object: MultiplePermissionsListener{

                override fun onPermissionRationaleShouldBeShown(
                    permissions: MutableList<com.karumi.dexter.listener.PermissionRequest>?,
                    token: PermissionToken?
                ) {

                }

                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {

                    if (report?.areAllPermissionsGranted()!!){
                        launchCamera()
                    }else{
                        showDialog()
                    }
                }

            })
            .check()
    }

    private fun showDialog() {
        AlertDialog.Builder(this@CaptureActivity)
            .setTitle(R.string.storage_permission_rationale_title)
            .setMessage(R.string.storage_permition_rationale_message)
            .setNegativeButton(android.R.string.cancel,
                { dialog, _ ->
                    dialog.dismiss()
                })
            .setPositiveButton(android.R.string.ok,
                { dialog, _ ->
                    dialog.dismiss()
                })
            .show()
    }

    private fun launchCamera() {

        val intent = Intent(this, CameraActivity2::class.java)
        startActivityForResult(intent, REQUEST_IMAGE_CAPTURE)
        /*val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (takePictureIntent.resolveActivity(packageManager) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
        }*/
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.e("Capture", "Este es l Resultado: " + Activity.RESULT_OK)
        if (resultCode == REQUEST_IMAGE_CAPTURE) {

            when (requestCode) {
                REQUEST_IMAGE_CAPTURE -> {

                    val extras = data?.getExtras()
                    val byteArray = extras?.get("result") as ByteArray
                    val measure = extras?.get("value") as Glucometer
                    val imageBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.size)
                    //val imageBitmap = extras?.get("result") as Bitmap


                    /*val matrix = Matrix()
                    if (imageBitmap.width > imageBitmap.height) {
                        matrix.postRotate(90F)
                    }
                    var bitmap = Bitmap.createBitmap(imageBitmap, 0, 0, imageBitmap.width, imageBitmap.height, matrix, false)*/
                    imgPhoto.setImageBitmap(imageBitmap)

                    //var strValue = "${measure.Sys} ${measure.Dia} ${measure.pulse}"
                    var strValue = "${measure.value}"


                    txtVNumber.setText(strValue)
                }
            }
        }
    }

    private fun cropImage(src: Bitmap){
        val mat = Mat()
        Utils.bitmapToMat(src, mat)

        mat.threshold(src) {
            mat.erosion(mat.toBitmap(), 10.0)
            mat.canny(mat.toBitmap()) {
                if (it.height > 20 && it.width > 20){
                    selectImage(src, it)
                }else {
                    imgPhoto.setImageBitmap(src)
                }
            }
        }


    }

    private fun selectImage(src: Bitmap, srcEdge: Bitmap){
        val mat = Mat()
        Utils.bitmapToMat(src, mat)
        mat.erosion(src, 3.0)
        mat.imageCrop(src, srcEdge)
        val bmp = mat.toBitmap()
        saveImage(bmp)
        cleanImage(bmp)

    }

    private fun cleanImage(src: Bitmap){
        val mat = Mat()
        mat.blackWhiteBitmap(src)
        var bmp = mat.toBitmap()
        if (mat.isActionable(bmp)) {
            bmp.cropBorders(bmp)
            mat.release()
            Utils.bitmapToMat(bmp, mat)
            mat.cropUp(mat.toBitmap())
            bmp = mat.toBitmap()
            mat.release()
            Utils.bitmapToMat(bmp, mat)
            mat.erosion(bmp, 5.0)
            //txtVNumber.setText(mat.getInfoMeasure(mat.toBitmap()))
            imgPhoto.setImageBitmap(mat.toBitmap())
        } else {
            imgPhoto.setImageBitmap(bmp)
        }
    }

    private fun applyGrayScale(src: Bitmap) {
        val mat = Mat()
        mat.toGray(src)
        imgPhoto.setImageBitmap(mat.toBitmap())
        mat.release()
    }

    private fun applyGaussianBlur(src: Bitmap){
        val mat = Mat()
        mat.gaussianBlur(src) { imgPhoto.setImageBitmap(it) }
        mat.release()
    }

    /**
     * Apply the Canny Edge Algorithm to detect edges of an image.
     */
    private fun applyCannyEdge(src: Bitmap) {
        val mat = Mat()
        mat.canny(src) { imgPhoto.setImageBitmap(it) }
    }

    /**
     * Apply the Threshold Algorithm.
     */
    private fun applyThreshold(src: Bitmap) {
        val mat = Mat()
        mat.threshold(src) { imgPhoto.setImageBitmap(it) }
    }

    private fun applyAdaptiveThreshold(src: Bitmap) {
        val mat = Mat()
        mat.adaptiveThreshold(src) { imgPhoto.setImageBitmap(it) }
    }

    private fun resetImage(src: Bitmap) {
        imgPhoto.setImageBitmap(src)
    }

    // Extension function to resize bitmap using new width value by keeping aspect ratio
    fun Bitmap.resizeByWidth(width:Int):Bitmap{
        val ratio:Float = this.width.toFloat() / this.height.toFloat()
        val height:Int = Math.round(width / ratio)

        return Bitmap.createScaledBitmap(
            this,
            width,
            height,
            false
        )
    }



    fun Bitmap.cropBitmap(src: Bitmap): Bitmap{

        var heightCrop = src.height.toDouble() * 0.48
        var widthCrop = src.width.toDouble() * 0.9

        val bmp = Bitmap.createBitmap(widthCrop.toInt(), heightCrop.toInt(), Bitmap.Config.ARGB_8888)

        var pixel = 0
        for (x in 0 until  widthCrop.toInt() ) {
            for (y in 0 until heightCrop.toInt() ) {
                // get pixel color
                pixel = src.getPixel(x, y)
                val A = Color.alpha(pixel)
                val R = Color.red(pixel)
                val G = Color.green(pixel)
                val B = Color.blue(pixel)

                // set new pixel color to output bitmap
                bmp.setPixel(x , y , Color.argb(A, R, G, B))
            }
        }

        return bmp
    }

    fun saveImage(bmp: Bitmap){
        val file_path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Reconog"
        val dir = File(file_path);
        if(!dir.exists()) {
            dir.mkdirs()
        }

        val file = File(dir, "img_0" + ".jpeg")
        val fOut = FileOutputStream(file)

        bmp.compress(Bitmap.CompressFormat.JPEG, 85, fOut)
        fOut.flush()
        fOut.close()

    }

}
