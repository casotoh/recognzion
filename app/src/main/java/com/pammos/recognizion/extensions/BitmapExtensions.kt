package com.pammos.recognizion.extensions

import android.graphics.*
import android.os.Environment
import android.util.Log
import org.opencv.android.Utils
import org.opencv.core.Mat
import org.opencv.core.MatOfPoint2f
import org.opencv.core.Point
import org.opencv.core.Rect
import org.opencv.imgcodecs.Imgcodecs
import org.opencv.imgproc.Imgproc
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

/**
 * Extension functions for [Bitmap].
 */
fun Bitmap.toMat() : Mat {
    val mat = Mat()
    Utils.bitmapToMat(this, mat)
    return mat
}

fun Bitmap.cropBitmap(src: Bitmap): Bitmap {

    var heightCrop = (src.height.toDouble() * 0.45).toInt()
    var widthCrop = (src.width.toDouble() * 0.75).toInt()

    var initHeight = (src.height.toDouble() * 0.1).toInt()
    var initWidth = (src.width.toDouble() * 0.25).toInt()

    val width = widthCrop - initWidth
    val height = heightCrop - initHeight

    val bmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)

    for (x in initWidth until  widthCrop ) {
        for (y in initHeight until heightCrop ) {
            // get pixel color
            val pixel = src.getPixel(x, y)
            val A = Color.alpha(pixel)
            val R = Color.red(pixel)
            val G = Color.green(pixel)
            val B = Color.blue(pixel)

            bmp.setPixel(x - initWidth , y - initHeight , Color.argb(A, R, G, B))
        }
    }

    return bmp
}

fun Bitmap.enhanceBlacks(tone: Int, range: Int): Bitmap{
    for (x in 0 until  this.width ) {
        for (y in 0 until this.height ) {
            // get pixel color
            val pixel = this.getPixel(x, y)
            val A = Color.alpha(pixel)
            var R = Color.red(pixel)
            var G = Color.green(pixel)
            var B = Color.blue(pixel)

            val res = (R + G + B) / 3
            if (res <= tone){
                //this.setPixel(x , y , Color.argb(A, 0, 0, 0))
            }else if(res > tone && res < (255 - range)){
                this.setPixel(x , y , Color.argb(A, R + range, G + range, B + range))
            }

        }
    }

    return this
}

fun Bitmap.adjustTone(limit: Int): Bitmap{
    for (x in 0 until  this.width ) {
        for (y in 0 until this.height ) {
            // get pixel color
            val pixel = this.getPixel(x, y)
            val A = Color.alpha(pixel)
            var R = Color.red(pixel)
            var G = Color.green(pixel)
            var B = Color.blue(pixel)

            val res = (R + G + B) / 3
            if (!(res < 5 || res >= limit)){
                this.setPixel(x , y , Color.argb(A, R - 5, G - 5, B - 5))
            }else if (res <= 250){
                this.setPixel(x , y , Color.argb(A, R + 5, G + 5, B + 5))
            }

        }
    }

    return this
}

fun identifyRed(src: Bitmap): Bitmap{
    val mat = Mat()

    for (x in 0 until  src.width ) {
        for (y in 0 until src.height ) {
            // get pixel color
            val pixel = src.getPixel(x, y)
            val A = Color.alpha(pixel)
            var R = Color.red(pixel)
            var G = Color.green(pixel)
            var B = Color.blue(pixel)

            if (R >= 200 && G < 230 && B < 200){
                src.setPixel(x , y , Color.argb(A, 0, 0, 0))
            }else{
                src.setPixel(x , y , Color.argb(A, 255, 255, 255))
            }

        }
    }

    mat.blackWhiteBitmap(src)
    return mat.toBitmap()
}

fun clearLeft(src: Bitmap, percentage: Double): Bitmap{
    var bmp = src

    val widthTemp = (src.width.toDouble() * percentage).toInt()
    for (x in 0 until widthTemp){
        for (y in 0 until src.height){
            bmp.setPixel(x, y, Color.argb(255, 255, 255, 255))
        }
    }

    return bmp
}

fun clearRight(src: Bitmap, percentage: Double): Bitmap{
    var bmp = src

    val widthTemp = src.width - (src.width.toDouble() * percentage).toInt()
    for (x in widthTemp until  src.width){
        for (y in 0 until src.height){
            bmp.setPixel(x, y, Color.argb(255, 255, 255, 255))
        }
    }

    return bmp
}


fun isImageCorrect(src: Bitmap): Boolean{
    val mat = Mat()
    var bmp = src
    var lines = 0

    for (i in 0 until 3){
        mat.release()
        Utils.bitmapToMat(bmp, mat)
        mat.cropUp(bmp)
        bmp = mat.toBitmap()
        val yPoint = verifyLines(bmp)
        if (yPoint > 0) {
            var recCrop = Rect(0, yPoint, bmp.width, bmp.height - yPoint)
            Utils.bitmapToMat(src, mat)
            mat.cropImage(src, recCrop)
            bmp = mat.toBitmap()
            lines += 1
        }
    }

    return lines > 1
}

fun isImageCorrectOximeter(src: Bitmap): Boolean{
    val mat = Mat()
    var bmp = src
    var lines = 0

    for (i in 0 until 2){
        mat.release()
        Utils.bitmapToMat(bmp, mat)
        mat.cropUp(bmp)
        bmp = mat.toBitmap()
        val yPoint = verifyLines(bmp)
        if (yPoint > 0) {
            var recCrop = Rect(0, yPoint, bmp.width, bmp.height - yPoint)
            Utils.bitmapToMat(src, mat)
            mat.cropImage(src, recCrop)
            bmp = mat.toBitmap()
            lines += 1
        }
    }

    return lines > 0
}

fun verifyLines(src: Bitmap): Int{

    var yPoint = 0
    for (y in 0 until  src.height ) {
        var total = 0
        for (x in 0 until src.width ) {
            val pixel = src.getPixel(x, y)
            val R = Color.red(pixel)
            total += R
        }
        if (total / src.width >= 235){
            if (y > 0) {
                yPoint = y
                break
            }

        }
    }

    return yPoint
}


fun Bitmap.resizeImageHeight(bitmap: Bitmap, mHeight: Int): Bitmap {
    val aspectRadio = bitmap.width.toDouble() / bitmap.height.toDouble()
    val mWidth = (aspectRadio * mHeight.toDouble()).toInt()
    val output = Bitmap.createBitmap(mWidth, mHeight, Bitmap.Config.ARGB_8888)
    val canvas = Canvas(output)
    val m = Matrix()
    m.setScale(mWidth.toFloat() / bitmap.width.toFloat(), mHeight.toFloat() / bitmap.height.toFloat())
    canvas.drawBitmap(bitmap, m, Paint())
    val mat = Mat()
    Utils.bitmapToMat(output, mat)

    return mat.toBitmap()
}

fun Bitmap. getLevelBrightness(): Int{
    var level = 0.0
    var sum = 0
    for (x in 0 until  this.width ) {
        for (y in 0 until this.height ) {
            // get pixel color
            val pixel = this.getPixel(x, y)
            val A = Color.alpha(pixel)
            var R = Color.red(pixel)
            var G = Color.green(pixel)
            var B = Color.blue(pixel)

            sum = sum + R + G + B

        }
    }

    level = (sum / (3 * (this.width * this.height))).toDouble()

    return level.toInt()
}

fun adjustImagesMicrolife(listBitmaps: ArrayList<Bitmap>): ArrayList<Bitmap> {
    var newList: ArrayList<Bitmap> = ArrayList()
    val mat = Mat()

    for (i in 0 until listBitmaps.size){
        Utils.bitmapToMat(listBitmaps[i], mat)
        mat.release()
        when (i) {
            0 -> {

                mat.straightenImage(listBitmaps[0], 6.0)
                mat.cropBorderRight(mat.toBitmap())
                mat.cropRightNumber(mat.toBitmap())
                var bmp = resizeImage(mat.toBitmap(), 70, 150)
                mat.release()
                Utils.bitmapToMat(bmp, mat)
            }
            1 -> {
                mat.cropBorderRight(listBitmaps[1])
                mat.straightenImage(mat.toBitmap(), 1.0)
                mat.cropRightNumber(mat.toBitmap())
                var bmp = resizeImage(mat.toBitmap(), 70, 150)
                mat.release()
                Utils.bitmapToMat(bmp, mat)
            }
            2 -> {

                mat.erosion(listBitmaps[2], 3.0)
                mat.cropBorderRight(mat.toBitmap())
                mat.cropRightNumber(mat.toBitmap())
            }
        }
        newList.add(mat.toBitmap())
    }

    return newList
}

fun adjustImagesOmron(listBitmaps: ArrayList<Bitmap>): ArrayList<Bitmap> {
    var newList: ArrayList<Bitmap> = ArrayList()
    val mat = Mat()

    for (i in 0 until listBitmaps.size){
        Utils.bitmapToMat(listBitmaps[i], mat)
        mat.release()
        when (i) {
            0 -> {

                var bmp = clearLeft(listBitmaps[0], 0.2)
                mat.cropBorderRight(bmp)
                mat.cropRightNumber(mat.toBitmap())
                mat.cropDown(mat.toBitmap())
                mat.erosion(mat.toBitmap(), 4.0)
                bmp = resizeImage(mat.toBitmap(), 70, 170)
                mat.release()
                Utils.bitmapToMat(bmp, mat)
            }
            1 -> {
                mat.cropBorderRight(listBitmaps[1])
                mat.cropRightNumber(mat.toBitmap())
                var bmp = resizeImage(mat.toBitmap(), 70, 150)
                mat.release()
                Utils.bitmapToMat(bmp, mat)
            }
            2 -> {
                mat.erosion(listBitmaps[2], 4.0)
                mat.cropBorderRight(mat.toBitmap())
                mat.cropRightNumber(mat.toBitmap())
            }
        }
        newList.add(mat.toBitmap())
    }

    return newList
}

fun saveImage(bmp: Bitmap){
    val file_path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/AReconog"
    val dir = File(file_path);
    if(!dir.exists()) {
        dir.mkdirs()
    }

    val date = Date()

    val formatter = SimpleDateFormat("yyMMddHHmmssSSS")
    val strDate = formatter.format(date)

    val file = File(dir, "$strDate.jpeg")
    val fOut = FileOutputStream(file)

    bmp.compress(Bitmap.CompressFormat.JPEG, 100, fOut)
    fOut.flush()
    fOut.close()

}

fun getPositionLineHorizontal(src: Bitmap, value: Int): Int{
    var yPoint = 0
    for (y in src.height - 1 downTo 0 ) {
        var total = 0
        for (x in 0 until src.width ) {
            val pixel = src.getPixel(x, y)
            val R = Color.red(pixel)
            total += R
        }
        if (total / src.width >= value){
            yPoint = y
            break

        }
    }

    return yPoint
}

fun Bitmap.white2Black(){
    for (x in 0 until  this.width ) {
        for (y in 0 until this.height ) {
            // get pixel color
            val pixel = this.getPixel(x, y)
            var R = Color.red(pixel)

            if (R == 255){
                this.setPixel(x, y, Color.argb(255, 0, 0, 0))
            }else{
                this.setPixel(x, y, Color.argb(255, 255, 255, 255))
            }
        }
    }
}

fun Bitmap.getValueDarker(): Int{
    var value = 255
    var xPos = 0
    var yPos = 0
    for (x in 1 until  this.width - 1) {
        for (y in 1 until this.height - 1) {
            // get pixel color
            var pixel = this.getPixel(x, y)
            var R = Color.red(pixel)

            if (value > R){
                xPos = x
                yPos = y
                value = R
            }
        }
    }

    return value
}


