package com.pammos.recognizion.extensions

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Color
import android.util.Log
import android.widget.Toast
import com.pammos.recognizion.Glucometer
import com.pammos.recognizion.Measurment
import com.pammos.recognizion.OxyMeasurment
import org.opencv.android.Utils
import org.opencv.core.Mat
import java.io.ByteArrayOutputStream


fun getValueMicrolife(src: Bitmap): Pair<ByteArray, Measurment>{

    var measurment: Measurment = Measurment(0, 0, 0)
    val stream = ByteArrayOutputStream()
    val mat = Mat()
    var bmp = src.cropBitmap(src)
    mat.imageSelect(bmp)
    bmp = mat.toBitmap()

    mat.cropRightSection(mat.toBitmap(), 0.3)

    bmp = mat.toBitmap()
    bmp = bmp.resizeImageHeight(bmp, 250)

    val levelBrightness = bmp.getLevelBrightness()
    Log.e("Brillo", "Este es el brillo: ${bmp.getLevelBrightness()}" )

    var mBmp = bmp.resizeImageHeight(bmp, 250)
    mat.release()
    mat.blackWhiteBitmap(bmp)

    if (mat.isActionable(mat.toBitmap())) {

        when {
            levelBrightness >= 140 -> bmp.enhanceBlacks(70, 20)
            levelBrightness >= 120 || levelBrightness > 140 -> bmp.enhanceBlacks(60, 30)
            levelBrightness >= 100 || levelBrightness > 120 -> bmp.enhanceBlacks(50, 40)
            levelBrightness >= 80 || levelBrightness > 100 -> bmp.enhanceBlacks(45, 50)
            levelBrightness >= 50 || levelBrightness < 80 -> bmp.enhanceBlacks(30, 60)
            levelBrightness < 50 -> bmp.enhanceBlacks(10, 40)
        }


        mat.blackWhiteBitmap(bmp)
        mat.straightenImage(mat.toBitmap(), 15.0)
        mat.dilate(mat.toBitmap(), 2.0)
        bmp = mat.toBitmap()

        bmp = bmp.resizeImageHeight(bmp, 250)

        mat.release()
        mat.erosion(bmp, 2.0)
        mat.cropUp(mat.toBitmap())
        mat.cropBorderUp(mat.toBitmap())
        mat.cropUp(mat.toBitmap())

        bmp = mat.toBitmap()

        mat.release()
        mat.cropDown(bmp)

        if (mat.rows() > 10 && mat.cols() > 10) {

            bmp = mat.toBitmap()
            mBmp = bmp

            mat.release()

            if (isImageCorrect(bmp)) {
                val listBitmap = mat.getInfoMeasure(bmp)
                if (listBitmap.size == 3){
                    val adjustList = adjustImagesMicrolife(listBitmap)
                    val listValues = getValueMeasure(adjustList)

                    measurment.Sys = listValues[0]
                    measurment.Dia = listValues[1]
                    measurment.pulse = listValues[2]
                }

                mBmp.compress(Bitmap.CompressFormat.JPEG, 100, stream)
                val image = stream.toByteArray()
                return Pair(image, measurment)
            } else {
                mBmp.compress(Bitmap.CompressFormat.JPEG, 100, stream)
                val image = stream.toByteArray()
                return Pair(image, measurment)
            }
        }else{
            mBmp.compress(Bitmap.CompressFormat.JPEG, 100, stream)
            val image = stream.toByteArray()
            return Pair(image, measurment)
        }
    }else{
        mBmp.compress(Bitmap.CompressFormat.JPEG, 100, stream)
        val image = stream.toByteArray()
        return Pair(image, measurment)
    }
}

fun getValueOmron(src: Bitmap): Pair<ByteArray, Measurment> {

    var mBmp = src
    var measurment = Measurment(0, 0, 0)
    val stream = ByteArrayOutputStream()
    val mat = Mat()

    var bmp = src.cropBitmap(src)
    saveImage(bmp)

    mat.toGray(bmp)

    bmp = mat.toBitmap()

    val levelBrightness = bmp.getLevelBrightness()
    Log.e("Brillo", "Este es el brillo: ${bmp.getLevelBrightness()}")

    when {
            levelBrightness >= 140 -> bmp.enhanceBlacks(140, 10)
            levelBrightness >= 110 || levelBrightness > 140 -> bmp.enhanceBlacks(120, 10)
            levelBrightness >= 80 || levelBrightness > 110 -> bmp.enhanceBlacks(100, 20)
            levelBrightness >= 50 || levelBrightness < 80 -> bmp.enhanceBlacks(60, 50)
            levelBrightness < 50 -> bmp.enhanceBlacks(45, 90)
        }


    mat.imageSelect(bmp)
    var bmpCrop = mat.toBitmap()

    mBmp = bmpCrop

    if (bmp.width != bmpCrop.width) {

        mat.cropLeftSection(bmpCrop, 0.3)
        mat.cropRightSection(mat.toBitmap(), 0.05)
        mat.cropUpSection(mat.toBitmap(), 0.15)
        mat.cropDownSection(mat.toBitmap(), 0.03)

        bmp = mat.toBitmap()
        bmp = bmp.resizeImageHeight(bmp, 250)


        mBmp = bmp

        mat.release()
        mat.blackWhiteBitmap(bmp)

        if (mat.isActionable(mat.toBitmap())) {

            mBmp = bmp
            mat.blackWhiteBitmap(bmp)
            bmp = mat.toBitmap()

            bmp = bmp.resizeImageHeight(bmp, 250)

            mat.release()
            mat.cropUp(bmp)
            mat.cropBorderUp(mat.toBitmap())
            mat.cropUp(mat.toBitmap())

            bmp = mat.toBitmap()

            mat.release()
            mat.cropDown(bmp)
            mat.erosion(mat.toBitmap(), 5.0)

            if (mat.rows() > 10 && mat.cols() > 10) {

                bmp = mat.toBitmap()

                mat.release()

                if (isImageCorrect(bmp)) {
                    val listBitmap = mat.getInfoMeasure(bmp)
                    if (listBitmap.size == 3) {
                        val adjustList = adjustImagesOmron(listBitmap)
                        val listValues = getValueMeasure(adjustList)

                        if (listValues.size >= 3) {
                            measurment.Sys = listValues[0]
                            measurment.Dia = listValues[1]
                            measurment.pulse = listValues[2]
                        }
                    }

                }
            }
        }
    }

    mBmp.compress(Bitmap.CompressFormat.JPEG, 100, stream)
    val image = stream.toByteArray()
    return Pair(image, measurment)
}

fun getValueOxyWatch(src: Bitmap): Pair<ByteArray, OxyMeasurment> {

    var mBmp = src
    var measurment = OxyMeasurment(0, 0)
    val stream = ByteArrayOutputStream()
    val mat = Mat()

    var bmp = src.cropBitmap(src)


    mat.imageSelect(bmp)

    if (bmp.height != mat.toBitmap().height) {
        bmp = mat.toBitmap()
        var bmpCrop = bmp

        mat.cropRightSection(bmp, 0.1)
        mat.cropLeftSection(mat.toBitmap(), 0.2)
        mat.cropUpSection(mat.toBitmap(), 0.2)
        mat.cropDownSection(mat.toBitmap(), 0.2)

        bmp = mat.toBitmap()
        bmp = identifyRed(bmp)
        bmp = bmp.resizeImageHeight(bmp, 250)

        mat.release()

        mat.erosion(bmp, 3.0)
        if (mat.isActionable(mat.toBitmap())) {
            mat.cropUp(bmp)
            mat.cropDown(mat.toBitmap())
            mat.cropBorderRight(mat.toBitmap())

            if (isImageCorrectOximeter(bmp)) {
                bmp = mat.toBitmap()
                val listBitmap = mat.getInfoMeasureOximeter(bmp)
                if (listBitmap.size >= 2) {
                    val listValues = getValueMeasure(listBitmap)

                    measurment.pulse = listValues[0]
                    measurment.oximeter = listValues[1]
                }
            }
        }
    }

    mBmp = bmp


    mBmp.compress(Bitmap.CompressFormat.JPEG, 100, stream)
    val image = stream.toByteArray()
    return Pair(image, measurment)
}

fun getValueSejoy(src: Bitmap): Pair<ByteArray, Measurment> {

    var mBmp = src
    var measurment = Measurment(0, 0, 0)
    val stream = ByteArrayOutputStream()
    val mat = Mat()

    var bmp = src.cropBitmap(src)

    mat.cropUpSection(bmp, 0.05)
    mat.cropDownSection(mat.toBitmap(), 0.05)

    bmp = mat.toBitmap()

    mat.imageSelect(bmp)
    mat.toGray(mat.toBitmap())

    //if (true) {
    if (bmp.height != mat.toBitmap().height) {
        bmp = mat.toBitmap()
        bmp = bmp.resizeImageHeight(bmp, 250)

        val levelBrightness = bmp.getLevelBrightness()
        measurment.Sys = levelBrightness
        //Log.e("Brillo", "Este es el brillo: ${bmp.getLevelBrightness()}" )

        //bmp.adjustTone(60)

        when {
            levelBrightness >= 140 -> bmp.enhanceBlacks(100, 10)
            levelBrightness in 130..139 -> bmp.enhanceBlacks(75, 20)
            levelBrightness in 120..129 -> bmp.enhanceBlacks(65, 20)
            levelBrightness in 100..119 -> bmp.enhanceBlacks(60, 20)
            levelBrightness in 80..99 -> bmp.enhanceBlacks(45, 30)
            levelBrightness in 70..79 -> bmp.enhanceBlacks(40, 40)
            levelBrightness in 60..69 -> bmp.enhanceBlacks(35, 50)
            levelBrightness in 50..59 -> bmp.enhanceBlacks(30, 60)
            levelBrightness < 50 -> bmp.enhanceBlacks(25, 80)
        }

        //saveImage(bmp)

        mat.release()
        mat.blackWhiteBitmap(bmp)
        //

        bmp = bmp.resizeImageHeight(mat.toBitmap(), 250)

        mat.cropRightSection(bmp, 0.3)
        mat.cropDown(mat.toBitmap())
        mat.cropBorderRight(mat.toBitmap())
        mat.cropUp(mat.toBitmap())
        mat.cropBorderUp(mat.toBitmap())
        mat.cropUp(mat.toBitmap())

        bmp = mat.toBitmap()
        mat.release()

        if (mat.isActionable(bmp)) {
            val listBitmap = mat.getInfoMeasureTensiometerSejoy(bmp)
            if (listBitmap.size > 2){
                val listValues = getValueMeasure(listBitmap)

                if (listValues.size >= 3) {
                    measurment.Sys = listValues[0]
                    measurment.Dia = listValues[1]
                    measurment.pulse = listValues[2]

                    Log.e("Medicion Sejoy:", "Sys: ${measurment.Sys} Dia: ${measurment.Dia} HR: ${measurment.pulse}")
                }
            }

        }
    }

    mBmp = bmp



    mBmp.compress(Bitmap.CompressFormat.JPEG, 100, stream)
    val image = stream.toByteArray()
    return Pair(image, measurment)
}

fun getValueGlucometerVivaChek(src: Bitmap): Pair<ByteArray, Glucometer>{

    var mBmp = src
    var glucometer = Glucometer(0)
    val stream = ByteArrayOutputStream()
    val mat = Mat()

    var bmp = src.cropBitmap(src)
    mat.cropUpSection(bmp, 0.1)
    mat.cropLeftSection(mat.toBitmap(), 0.01)
    mat.cropRightSection(mat.toBitmap(), 0.01)
    mat.cropDownSection(mat.toBitmap(), 0.1)
    bmp = mat.toBitmap()
    mBmp = bmp
    mat.release()

    mat.imageSelect(bmp)
    mat.toGray(mat.toBitmap())
    mat.cropDownSection(mat.toBitmap(), 0.25)
    mat.cropUpSection(mat.toBitmap(), 0.3)

    val bmpTemp = mat.toBitmap()
    var percentageBrightness = 0.45
    val toneDarker = bmpTemp.getValueDarker()
    val bright = bmpTemp.getLevelBrightness()

    if (percentageBrightness <= 100){
        percentageBrightness = 0.6
    }

    var matDetc = detectRangeColor(mat, toneDarker.toDouble(), (bright.toDouble()  * percentageBrightness))

    bmp = matDetc.toBitmap()
    bmp.white2Black()
    mat.release()

    mat.cropDown(bmp)
    mat.cropUp(mat.toBitmap())
    mat.cropBorderRight(mat.toBitmap())
    mat.erosion(mat.toBitmap(), 2.0)
    bmp = mat.toBitmap()

    if (mat.isActionable(bmp)) {
        var listBitmap: ArrayList<Bitmap> = ArrayList()
        listBitmap.add(bmp)
        val listValues = getValueMeasure(listBitmap)
        if (listValues.size == 1){
            glucometer.value = listValues[0]
        }

    }

    mBmp.compress(Bitmap.CompressFormat.JPEG, 100, stream)
    val image = stream.toByteArray()
    return Pair(image, glucometer)
}
