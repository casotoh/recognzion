package com.pammos.recognizion.extensions

import android.graphics.*
import android.util.Log
import org.opencv.android.Utils
import org.opencv.core.*
import org.opencv.core.Point
import org.opencv.core.Rect
import org.opencv.imgproc.Imgproc


/**
 * Extension functions for [Mat].
 */
fun Mat.toGray(bitmap: Bitmap) {
    Utils.bitmapToMat(bitmap, this)
    Imgproc.cvtColor(this, this, Imgproc.COLOR_RGB2GRAY)
}

fun Mat.dilate(bitmap: Bitmap, sizePoint: Double) {
    Utils.bitmapToMat(bitmap, this)
    Imgproc.dilate(this, this, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, Size(sizePoint, sizePoint)))

}

fun Mat.dilate2(bitmap: Bitmap, sizeWidth: Double, sizeHeight: Double) {
    Utils.bitmapToMat(bitmap, this)
    Imgproc.dilate(this, this, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, Size(sizeWidth, sizeHeight)))

}

fun Mat.erosion(bitmap: Bitmap, sizePoint: Double) {
    Utils.bitmapToMat(bitmap, this)
    Imgproc.erode(this, this, Imgproc.getStructuringElement(Imgproc.MORPH_ERODE, Size(sizePoint,sizePoint)))
}

fun Mat.erosion2(bitmap: Bitmap, sizeWidth: Double, sizeHeight: Double) {
    Utils.bitmapToMat(bitmap, this)
    Imgproc.erode(this, this, Imgproc.getStructuringElement(Imgproc.MORPH_ERODE, Size(sizeWidth, sizeHeight)))
}

fun Mat.gaussianBlur(bitmap: Bitmap, kSize: Size = Size(125.toDouble(), 125.toDouble()), sigmaX:Double = 0.toDouble(), block: (Bitmap) -> Unit) {
    Utils.bitmapToMat(bitmap, this)
    Imgproc.GaussianBlur(this, this, kSize, sigmaX)
    return block(this.toBitmap())
}

fun Mat.soften(bitmap: Bitmap){
    Utils.bitmapToMat(bitmap, this)
    Imgproc.blur(this, this, Size(5.0, 5.0))
    //Imgproc.medianBlur(this, this, 7)
}

fun Mat.straightenImage(src: Bitmap, pointMove: Double){
    val srcImage = Mat()
    Utils.bitmapToMat(src, srcImage)
    Utils.bitmapToMat(src, this)

    val mSrc = MatOfPoint2f(Point(pointMove,0.0), Point(0.0, (src.height - 1).toDouble()),
            Point((src.width - 1).toDouble(), 0.0), Point((src.width - pointMove).toDouble(), (src.height - 1).toDouble()))
    val mDest = MatOfPoint2f(Point(0.0, 0.0), Point(0.0, (src.height - 1).toDouble()),
            Point((src.width - 1).toDouble(), 0.0), Point((src.width - 1).toDouble(), (src.height - 1).toDouble()))

    val transform = Imgproc.getPerspectiveTransform(mSrc, mDest)
    Imgproc.warpPerspective(srcImage, this, transform, this.size())

}

fun Mat.canny(bitmap: Bitmap, threshold1: Double = 20.toDouble(), threshold2: Double = 255.toDouble(), block: (Bitmap) -> Unit) {
    this.toGray(bitmap)
    Imgproc.Canny(this, this, threshold1, threshold2)
    return block(this.toBitmap())
}

fun Mat.threshold(bitmap: Bitmap, thresh: Double = 50.toDouble(), maxVal: Double = 255.toDouble(), type:Int = Imgproc.THRESH_BINARY, block: (Bitmap) -> Unit) {
    this.toGray(bitmap)
    Imgproc.threshold(this, this, thresh, maxVal, type)
    return block(this.toBitmap())
}

fun Mat.adaptiveThreshold(bitmap: Bitmap, maxValue: Double = 255.toDouble(),
                                  adaptiveMethod: Int = Imgproc.ADAPTIVE_THRESH_MEAN_C,
                                  thresholdType: Int = Imgproc.THRESH_BINARY,
                                  blockSize: Int = 15,
                                  c: Double = 12.toDouble(), block: (Bitmap) -> Unit) {
    this.toGray(bitmap)
    Imgproc.adaptiveThreshold(this, this, maxValue, adaptiveMethod, thresholdType, blockSize, c)
    return block(this.toBitmap())
}

fun Mat.blackWhiteBitmap(bitmap: Bitmap){
    this.toGray(bitmap)
    Imgproc.threshold(this, this, 90.0, 255.0, Imgproc.THRESH_BINARY)
}

fun Mat.toBitmap(config: Bitmap.Config = Bitmap.Config.ARGB_8888): Bitmap {
    val bitmap = Bitmap.createBitmap(this.cols(), this.rows(), config)
    Utils.matToBitmap(this, bitmap)
    return bitmap
}

fun Mat.imageCrop(src: Bitmap, bmpEdge: Bitmap){

    val matEdge = Mat()
    Utils.bitmapToMat(bmpEdge, matEdge)
    val imgSource = matEdge.clone()

    val imageHSV = Mat(imgSource.size(), CvType.CV_8UC4)
    val imageBlurr = Mat(imgSource.size(), CvType.CV_8UC4)
    val imageA = Mat(imgSource.size(), CvType.CV_32F)
    Imgproc.cvtColor(imgSource, imageHSV, Imgproc.COLOR_BGR2GRAY)
    Imgproc.GaussianBlur(imageHSV, imageBlurr, Size(5.0, 5.0), 0.0)
    Imgproc.adaptiveThreshold(imageBlurr, imageA, 255.0, Imgproc.ADAPTIVE_THRESH_MEAN_C, Imgproc.THRESH_BINARY, 7, 5.0)

    val contours = ArrayList<MatOfPoint>()
    Imgproc.findContours(imageA, contours, Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE)

    var recCrop = Rect(0, 0, bmpEdge.width, bmpEdge.height)
    Log.e("Extensiones", "Ancho: " + bmpEdge.width + ", Alto: " + bmpEdge.height)

    for (i in contours.indices) {
        if (Imgproc.contourArea(contours[i]) > 2000) {
            val rect = Imgproc.boundingRect(contours[i])
            val limMin = (bmpEdge.width.toDouble() * 0.30).toInt()
            val limMax = (bmpEdge.width.toDouble() * 0.55).toInt()

            if (rect.height > limMin && rect.height < limMax && rect.width > limMin && rect.width < limMax) {

                recCrop = Rect(rect.x + 2, rect.y + 15, (rect.width.toDouble() * 0.75).toInt(), rect.height - 12)

            }
        }
    }

    Utils.bitmapToMat(src, this)
    val cropped = Mat(this, recCrop)
    Imgproc.cvtColor(cropped, this, Imgproc.COLOR_RGB2GRAY)

}

fun Mat.imageSelect(src: Bitmap){

    val matEdge = Mat()
    Utils.bitmapToMat(src, matEdge)
    val imgSource = matEdge.clone()

    val imageHSV = Mat(imgSource.size(), CvType.CV_8UC4)
    val imageBlurr = Mat(imgSource.size(), CvType.CV_8UC4)
    val imageA = Mat(imgSource.size(), CvType.CV_32F)

    Imgproc.cvtColor(imgSource, imageHSV, Imgproc.COLOR_BGR2GRAY)
    Imgproc.GaussianBlur(imageHSV, imageBlurr, Size(5.0, 5.0), 0.0)
    Imgproc.adaptiveThreshold(imageBlurr, imageA, 255.0, Imgproc.ADAPTIVE_THRESH_MEAN_C, Imgproc.THRESH_BINARY, 9, 5.0)

    val bmpImgHSV = imageHSV.toBitmap()
    val bmpImgBlurr = imageBlurr.toBitmap()
    val bmpImgA = imageA.toBitmap()

    val contours = ArrayList<MatOfPoint>()
    Imgproc.findContours(imageA, contours, Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE)

    var recCrop = Rect(0, 0, src.width, src.height)
    Log.e("Extensiones", "Ancho: " + src.width + ", Alto: " + src.height)

    val limMin = (src.width.toDouble() * 0.6).toInt()
    val limMax = (src.height.toDouble() * 0.9).toInt()

    for (i in contours.indices) {
        var counterD = contours[i]

        if (Imgproc.contourArea(contours[i]) > 1000) {
            val rect = Imgproc.boundingRect(contours[i])


            if (rect.height > limMin && rect.height < limMax && rect.width > limMin && rect.width < limMax) {

                recCrop = Rect(rect.x, rect.y, rect.width, rect.height)

            }
        }
    }

    Utils.bitmapToMat(src, this)
    val cropped = Mat(this, recCrop)
    Imgproc.cvtColor(cropped, this, Imgproc.COLOR_RGBA2RGB)
}

fun Mat.cropRightSection(src: Bitmap, percentage: Double){
    var xPoint = (src.width.toDouble() * percentage).toInt()

    var recCrop = Rect(xPoint, 0, src.width - xPoint, src.height)
    cropImage(src, recCrop)
}

fun Mat.cropLeftSection(src: Bitmap, percentage: Double){
    var xPoint = src.width - (src.width.toDouble() * percentage).toInt()

    var recCrop = Rect(0, 0, xPoint, src.height)
    cropImage(src, recCrop)
}

fun Mat.cropUpSection(src: Bitmap, percentage: Double){
    var yPoint = (src.height.toDouble() * percentage).toInt()

    var recCrop = Rect(0, yPoint, src.width, src.height - yPoint)
    cropImage(src, recCrop)
}

fun Mat.cropDownSection(src: Bitmap, percentage: Double){
    var yPoint = src.height - (src.height.toDouble() * percentage).toInt()

    var recCrop = Rect(0, 0, src.width,  yPoint)
    cropImage(src, recCrop)
}

fun Bitmap.cropBorders(src: Bitmap){

    val width = (src.width.toDouble() * 0.2).toInt()
    val height = src.height
    for (x in 0 until  width ) {

        for (y in 0 until height ) {
            src.setPixel(x, y, Color.argb(255, 255, 255, 255))
        }
    }

}

fun Mat.cropBorderUp(src: Bitmap){

    val mHeight = (src.height.toDouble() * 0.1).toInt()
    var yPoint = 0
    for (y in 0 until  mHeight ) {
        var total = 0
        for (x in 0 until src.width ) {
            val pixel = src.getPixel(x, y)
            val R = Color.red(pixel)
            if (R == 0) {
                total += 1
            }
        }
        if ((total.toDouble() / src.width.toDouble()) > 0.6){
            yPoint = y
        }
    }


    yPoint += 3

    var recCrop = Rect(0, yPoint, src.width, src.height - yPoint)
    cropImage(src, recCrop)
}

fun Mat.cropBorderRight(src: Bitmap){

    val mat = Mat()
    Utils.bitmapToMat(src, mat)
    mat.dilate(mat.toBitmap(), 4.0)
    val bmp = mat.toBitmap()

    var xPoint = bmp.width
    for (x in bmp.width - 1 downTo  0 ) {
        var total = 0
        for (y in 0 until bmp.height ) {
            val pixel = bmp.getPixel(x, y)
            val R = Color.red(pixel)
            if (R == 0) {
                total += 1
            }
        }
        if ((total.toDouble() / bmp.width.toDouble()) > 0.0){
            if (x < bmp.width - 4 ) {
                xPoint = x + 2
                break
            }
        }
    }

    var recCrop = Rect(0, 0, xPoint, src.height)
    cropImage(src, recCrop)

}

fun Mat.cropBorderLeft(src: Bitmap){

    val widthTemp = (src.width.toDouble() * 0.15).toInt()

    var xPoint = 0
    for (x in 0 until widthTemp) {
        var total = 0
        for (y in 0 until src.height ) {
            val pixel = src.getPixel(x, y)
            val R = Color.red(pixel)
            if (R == 0) {
                total += 1
            }
        }
        if ((total.toDouble() / src.width.toDouble()) > 0.1){
            if (x > 0 ) {
                xPoint = x
                break
            }
        }
    }

    var recCrop = Rect(xPoint, 0, src.width - xPoint, src.height)
    cropImage(src, recCrop)

}

fun Mat.cropUp(src: Bitmap){

    val mat = Mat()
    Utils.bitmapToMat(src, mat)
    mat.dilate2(mat.toBitmap(), 8.0, 1.0)
    val bmp = mat.toBitmap()

    var yPoint = 0
    for (y in 0 until  bmp.height ) {
        var total = 0
        for (x in 0 until bmp.width ) {
            val pixel = bmp.getPixel(x, y)
            val R = Color.red(pixel)
            total += R
        }
        if (total / bmp.width < 240){
            if (y > 0) {
                yPoint = y
            }
            break
        }
    }

    var recCrop = Rect(0, yPoint, src.width, src.height - yPoint)
    cropImage(src, recCrop)
}

fun Mat.cropDown(src: Bitmap){

    val mat = Mat()
    Utils.bitmapToMat(src, mat)
    mat.dilate2(mat.toBitmap(), 7.0, 1.0)
    val bmp = mat.toBitmap()

    var yPoint = 0
    for (y in bmp.height - 1 downTo 0 ) {
        var total = 0
        for (x in 0 until bmp.width ) {
            val pixel = bmp.getPixel(x, y)
            val R = Color.red(pixel)
            total += R
        }
        if (total / bmp.width < 255){
            yPoint = y
            break

        }
    }
    var recCrop = Rect(0, 0, src.width, yPoint)
    cropImage(src, recCrop)
}

fun Mat.cropRightNumber(src: Bitmap){

    val mat = Mat()
    Utils.bitmapToMat(src, mat)
    val bmp = mat.toBitmap()

    var xPoint = bmp.width
    for (x in bmp.width - 1 downTo  0 ) {
        var total = 0
        for (y in 0 until bmp.height ) {
            val pixel = bmp.getPixel(x, y)
            val R = Color.red(pixel)
            if (R == 0) {
                total += 1
            }
        }
        if ((total.toDouble() / bmp.width.toDouble()) > 0.1){
            xPoint = x
            break
        }
    }

    var recCrop = Rect(0, 0, xPoint, src.height)
    cropImage(src, recCrop)

}


fun Mat.getInfoMeasure(src: Bitmap): ArrayList<Bitmap>{
    var listBitmap: ArrayList<Bitmap> = ArrayList()
    Utils.bitmapToMat(src, this)
    if (this.isActionable(src)) {
        val sysBitmap = getBitmapSys(src)
        if (sysBitmap.second != src.height) {
            listBitmap.add(sysBitmap.first)
            val diaBitmap = getBitmapDia(src, sysBitmap.second)
            if (diaBitmap.second != src.height) {
                listBitmap.add(diaBitmap.first)
                val pulseBitmap = getBitmapPulse(src)
                if (pulseBitmap.height != src.height) {
                    listBitmap.add(pulseBitmap)
                }
            }
        }
    }
    return listBitmap
}

fun Mat.getInfoMeasureOximeter(src: Bitmap): ArrayList<Bitmap>{
    var listBitmap: ArrayList<Bitmap> = ArrayList()
    Utils.bitmapToMat(src, this)
    if (this.isActionable(src)) {
        val bitmapPulse = getBitmapPulseOximeter(src)
        if (bitmapPulse.height > 30){
            listBitmap.add(bitmapPulse)
            val bitmapOximeter = getBitmapSpO2(src)
            if (bitmapOximeter.height > 30){
                listBitmap.add(bitmapOximeter)
            }
        }
    }
    return listBitmap
}

fun Mat.getInfoMeasureTensiometerSejoy(src: Bitmap): ArrayList<Bitmap>{
    var listBitmap: ArrayList<Bitmap> = ArrayList()
    Utils.bitmapToMat(src, this)
    if (this.isActionable(src)) {
        val sysBitmap = getBitmapSysSejoy(src)
        if (sysBitmap.height != src.height && sysBitmap.height > 60) {
            listBitmap.add(sysBitmap)
            val diaBitmap = getBitmapDiaSejoy(src)
            if (diaBitmap.height != src.height && diaBitmap.height > 60) {
                listBitmap.add(diaBitmap)
                val pulseBitmap = getBitmapPulseSejoy(src)
                if (pulseBitmap.height != src.height && pulseBitmap.height > 60) {
                    listBitmap.add(pulseBitmap)
                }
            }
        }
    }
    return listBitmap
}


fun getBitmapSys(src: Bitmap): Pair<Bitmap, Int>{
    val mat = Mat()
    Utils.bitmapToMat(src, mat)
    mat.erosion(src, 4.0)
    var bmp = mat.toBitmap()
    mat.release()
    var yPoint = bmp.height
    for (y in 0 until  bmp.height ) {
        var total = 0
        for (x in 0 until bmp.width ) {
            val pixel = bmp.getPixel(x, y)
            val R = Color.red(pixel)
            total += R
        }
        if (total / bmp.width > 240){
            if (y > 0) {
                yPoint = y
                break
            }

        }
    }

    var recCrop = Rect(0, 0, src.width, yPoint)
    mat.cropImage(src, recCrop)
    bmp = mat.toBitmap()
    mat.release()

    mat.cropUp(bmp)
    mat.dilate(mat.toBitmap(), 3.0)
    if (mat.rows() > 10 && mat.cols() > 10) {
        bmp = mat.toBitmap()
        bmp = bmp.resizeImageHeight(bmp, 70)
    }
    return Pair(bmp, yPoint)
}

fun getBitmapDia(src: Bitmap, yPos: Int): Pair<Bitmap, Int>{
    val mat = Mat()
    Utils.bitmapToMat(src, mat)
    var recCrop = Rect(0, yPos + 3, src.width, src.height - (yPos + 3))
    mat.cropImage(src, recCrop)
    mat.cropUp(mat.toBitmap())
    val bmpFinal = mat.toBitmap()
    mat.erosion(mat.toBitmap(), 3.0)
    var bmp = mat.toBitmap()

    var yPoint = bmp.height
    for (y in 0 until  bmp.height ) {
        var total = 0
        for (x in 0 until bmp.width ) {
            val pixel = bmp.getPixel(x, y)
            val R = Color.red(pixel)
            if(R == 255) {
                total += 1
            }
        }
        if ((total.toDouble() / bmp.width.toDouble()) > 0.90){
            if (y > 0) {
                yPoint = y
            }
            break
        }
    }
    mat.release()

    recCrop = Rect(0, 0, bmp.width, yPoint)
    mat.release()
    mat.cropImage(bmpFinal, recCrop)
    bmp = mat.toBitmap()
    mat.release()
    mat.erosion(bmp, 2.0)
    bmp = mat.toBitmap()
    bmp = bmp.resizeImageHeight(bmp, 70)
    return Pair(bmp, yPos + yPoint)

}

fun getBitmapPulse(src: Bitmap): Bitmap{
    val mat = Mat()
    Utils.bitmapToMat(src, mat)
    mat.cropPulse(src, 5.0)
    mat.cropRight(mat.toBitmap())
    mat.cropUp(mat.toBitmap())
    var bmp = mat.toBitmap()
    mat.release()
    mat.erosion(bmp, 1.0)
    bmp = mat.toBitmap()
    bmp = bmp.resizeImageHeight(bmp, 70)
    return  bmp
}

fun Mat.cropPulse(src: Bitmap, pointErosion: Double){

    val mat = Mat()
    Utils.bitmapToMat(src, mat)
    mat.erosion(src, pointErosion)
    val bmp = mat.toBitmap()

    var yPoint = getPositionLineHorizontal(bmp, 240)
    var recCrop = Rect(0, yPoint, src.width, src.height - yPoint)
    cropImage(src, recCrop)
}

// Obtener el Pulso de Tensiometro Sejoy
fun getBitmapPulseSejoy(src: Bitmap): Bitmap{
    val mat = Mat()
    Utils.bitmapToMat(src, mat)
    mat.cropPulse(src, 1.0)
    mat.cropUp(mat.toBitmap())
    var bmp = mat.toBitmap()
    bmp = clearRight(bmp, 0.03)
    mat.release()
    mat.cropRightNumber(bmp)
    mat.straightenImage(mat.toBitmap(), 2.0)
    mat.erosion(mat.toBitmap(), 1.0)
    bmp = mat.toBitmap()
    bmp = bmp.resizeImageHeight(bmp, 70)
    return  bmp
}

fun getBitmapDiaSejoy(src: Bitmap): Bitmap{
    val mat = Mat()
    Utils.bitmapToMat(src, mat)

    var yPoint = getPositionLineHorizontal(src, 240)
    if (yPoint == 0){
        yPoint = src.height
    }
    var rectCrop = Rect(0, 0, src.width, yPoint)
    mat.cropImage(src, rectCrop)
    mat.cropDown(mat.toBitmap())

    var bmp = mat.toBitmap()
    mat.release()

    yPoint = getPositionLineHorizontal(bmp, 240)

    rectCrop = Rect(0, yPoint, bmp.width, bmp.height - yPoint)
    mat.cropImage(bmp, rectCrop)

    bmp = mat.toBitmap()
    bmp = clearRight(bmp, 0.05)

    mat.release()
    mat.cropRightNumber(bmp)
    mat.straightenImage(mat.toBitmap(), 2.0)
    mat.erosion(mat.toBitmap(), 1.0)
    bmp = mat.toBitmap()
    bmp = bmp.resizeImageHeight(bmp, 70)

    return  bmp
}

fun getBitmapSysSejoy(src: Bitmap): Bitmap{
    val mat = Mat()
    Utils.bitmapToMat(src, mat)

    mat.dilate(src, 1.0)

    var yPoint = 0
    for (y in 0 until mat.toBitmap().height ) {
        var total = 0
        for (x in 0 until mat.toBitmap().width ) {
            val pixel = mat.toBitmap().getPixel(x, y)
            val R = Color.red(pixel)
            total += R
        }
        if (total / mat.toBitmap().width >= 240){
            yPoint = y
            break

        }
    }

    var recCrop = Rect(0, 0, src.width, yPoint)
    mat.cropImage(src, recCrop)
    var bmp = mat.toBitmap()
    mat.release()

    mat.cropUp(bmp)
    mat.cropDown(mat.toBitmap())

    bmp = mat.toBitmap()
    bmp = clearRight(bmp, 0.05)
    mat.cropRightNumber(bmp)
    mat.straightenImage(mat.toBitmap(), 2.0)
    bmp = bmp.resizeImageHeight(mat.toBitmap(), 70)

    return bmp
}

fun getBitmapPulseOximeter(src: Bitmap): Bitmap{
    val mat = Mat()
    Utils.bitmapToMat(src, mat)
    mat.erosion(src, 10.0)

    var bmp = mat.toBitmap()

    var yPoint = bmp.height
    for (y in 0 until bmp.height) {
        var total = 0
        for (x in 0 until bmp.width ) {
            val pixel = bmp.getPixel(x, y)
            val R = Color.red(pixel)
            total += R
        }
        if (total / bmp.width >= 250){
            yPoint = y
            break

        }
    }
    var recCrop = Rect(0, 0, src.width, yPoint)
    mat.cropImage(src, recCrop)

    bmp = mat.toBitmap()
    mat.release()
    mat.cropUp(bmp)
    mat.cropDown(mat.toBitmap())
    mat.erosion(mat.toBitmap(), 6.0)
    return  mat.toBitmap()
}

fun getBitmapSpO2(src: Bitmap): Bitmap{

    val heightTemp = (src.height.toDouble() * 0.8).toInt()
    val mat = Mat()
    Utils.bitmapToMat(src, mat)
    mat.erosion(src, 10.0)

    var bmp = mat.toBitmap()

    var yPoint = 0
    for (y in heightTemp downTo  0) {
        var total = 0
        for (x in 0 until bmp.width ) {
            val pixel = bmp.getPixel(x, y)
            val R = Color.red(pixel)
            total += R
        }
        if (total / bmp.width >= 250){
            yPoint = y
            break

        }
    }
    var recCrop = Rect(0, yPoint, src.width, src.height - yPoint)
    mat.cropImage(src, recCrop)

    bmp = mat.toBitmap()
    mat.release()
    mat.cropBorderRight(bmp)
    mat.cropRightOximeter(mat.toBitmap())
    mat.cropUp(mat.toBitmap())
    mat.cropDown(mat.toBitmap())
    mat.erosion(mat.toBitmap(), 6.0)
    mat.cropBorderRight(mat.toBitmap())
    return  mat.toBitmap()
}

fun Mat.cropRightOximeter(src: Bitmap){
    if (src.width > 10 && src.height > 10) {

        val widthTemp = (src.width.toDouble() * 0.9).toInt()
        var xPoint = src.width
        for (x in widthTemp downTo 0) {
            var total = 0
            for (y in 0 until src.height) {
                val pixel = src.getPixel(x, y)
                val R = Color.red(pixel)
                total += R
            }
            if (total / src.height >= 250) {
                xPoint = x
                break
            }
        }

        var recCrop = Rect(0, 0, xPoint, src.height)
        cropImage(src, recCrop)
    }
}

fun Mat.cropRight(src: Bitmap){
    if (src.width > 10 && src.height > 10) {
        var xPoint = src.width

        for (x in src.width - 1 downTo 0) {
            var total = 0
            for (y in 0 until src.height) {
                val pixel = src.getPixel(x, y)
                val R = Color.red(pixel)
                total += R
            }
            if (total / src.height < 200) {
                xPoint = x + 2
                break
            }
        }
        if (xPoint > src.width) {
            xPoint = src.width
        }
        var recCrop = Rect(0, 0, xPoint, src.height)
        cropImage(src, recCrop)
    }
}

fun Bitmap.getValue(): Int{
    var value = 0
    val mat = Mat()
    Utils.bitmapToMat(this, mat)
    var bmp = mat.toBitmap()
    var widthNumber = 10
    for (i in 0 until 3){
        if (bmp.width > 10 && bmp.height > 10) {
            mat.cropRight(bmp)
            bmp = mat.toBitmap()
            val value7seg = getDigit7Seg(bmp)
            widthNumber = value7seg.second
            value += getDecimalNumber(i, value7seg.first)
            var valueWidth = bmp.width - widthNumber
            if (valueWidth <= 0) {
                valueWidth = 10
            }
            val rectCrop = Rect(0, 0, valueWidth, bmp.height)
            mat.cropImage(bmp, rectCrop)
            bmp = mat.toBitmap()
        }
    }
    return value
}

// Metodos para obtener los valores de las medicones realizadas
fun getValueMeasure(list: ArrayList<Bitmap>): ArrayList<Int>{
    var listValues: ArrayList<Int> = ArrayList()
    if (list.size > 0){
        for (i in 0 until list.size){
            listValues.add(list[i].getValue())
        }
    }

    return listValues
}

fun getDigit7Seg(src: Bitmap): Pair<Int, Int>{
    val mat = Mat()
    Utils.bitmapToMat(src, mat)
    mat.dilate(src, 3.0)
    var bmp = mat.toBitmap()
    var xPoint = bmp.width
    for (x in bmp.width - 1 downTo 0) {
        var total = 0
        for (y in 0 until bmp.height ) {
            val pixel = bmp.getPixel(x, y)
            val R = Color.red(pixel)
            if(R == 255) {
                total += 1
            }
        }
        if ((total.toDouble() / src.height.toDouble()) > 0.99){
            if (x < (src.width - (src.height.toDouble() * 0.53).toInt())) {
                xPoint = x
                break
            }

        }
    }
    if (xPoint == src.width){xPoint = 0}

    var recCrop = Rect(xPoint, 0, src.width - xPoint, src.height)
    mat.cropImage(src, recCrop)
    mat.erosion(mat.toBitmap(), 3.0)
    mat.cropBorderLeft(mat.toBitmap())
    mat.cropRightNumber(mat.toBitmap())
    bmp = mat.toBitmap()
    bmp = resizeImage(bmp, 70, 50)
    return Pair(mat.getNumber(bmp), src.width - xPoint)
}

fun resizeImage(bitmap: Bitmap, wantedHeight: Int, wantedWidth: Int): Bitmap {
    val output = Bitmap.createBitmap(wantedWidth, wantedHeight, Bitmap.Config.ARGB_8888)
    val canvas = Canvas(output)
    val m = Matrix()
    m.setScale(wantedWidth.toFloat() / bitmap.width.toFloat(), wantedHeight.toFloat() / bitmap.height.toFloat())
    canvas.drawBitmap(bitmap, m, Paint())
    val mat = Mat()
    Utils.bitmapToMat(output, mat)

    return mat.toBitmap()
}

fun Mat.getNumber(src: Bitmap): Int{
    var seg: Byte = 0x00

    if (src.height > 10 && src.width > 10) {
        for (i in 0 until 8) {
            when (i) {
                0 -> {
                    val xPoint = (src.width.toDouble() * 0.5).toInt()
                    val yPoint = (src.height.toDouble() * 0.08).toInt()

                    var count = 0
                    for (i in -3 until 4) {
                        for (j in -1 until 2) {
                            val pixel = src.getPixel(xPoint + i, yPoint + j)
                            val R = Color.red(pixel)
                            if (R < 255){count += 1}
                            src.setPixel(xPoint + i, yPoint + j, Color.argb(255, 255, 0, 0))
                        }
                    }

                    if (count >= 10){
                        seg = (seg.toInt() or 0x01).toByte()
                    }
                }
                1 -> {
                    val xPoint = (src.width.toDouble() * 0.9).toInt()
                    val yPoint = (src.height.toDouble() * 0.28).toInt()

                    var count = 0
                    for (i in -1 until 2) {
                        for (j in -3 until 4) {
                            val pixel = src.getPixel(xPoint + i, yPoint + j)
                            val R = Color.red(pixel)
                            if (R < 255){count += 1}
                            src.setPixel(xPoint + i, yPoint + j, Color.argb(255, 255, 0, 0))
                        }
                    }

                    if (count >= 10){
                        seg = (seg.toInt() or 0x02).toByte()
                    }
                }
                2 -> {
                    val xPoint = (src.width.toDouble() * 0.9).toInt()
                    val yPoint = (src.height.toDouble() * 0.72).toInt()

                    var count = 0
                    for (i in -1 until 2) {
                        for (j in -3 until 4) {
                            val pixel = src.getPixel(xPoint + i, yPoint + j)
                            val R = Color.red(pixel)
                            if (R < 255){count += 1}
                            src.setPixel(xPoint + i, yPoint + j, Color.argb(255, 255, 0, 0))
                        }
                    }
                    if (count >= 10){
                        seg = (seg.toInt() or 0x04).toByte()
                    }
                }
                3 -> {
                    val xPoint = (src.width.toDouble() * 0.5).toInt()
                    val yPoint = (src.height.toDouble() * 0.92).toInt()

                    var count = 0
                    for (i in -3 until 4) {
                        for (j in -1 until 2) {
                            val pixel = src.getPixel(xPoint + i, yPoint + j)
                            val R = Color.red(pixel)
                            if (R < 255){count += 1}
                            src.setPixel(xPoint + i, yPoint + j, Color.argb(255, 255, 0, 0))
                        }
                    }

                    if (count >= 10){
                        seg = (seg.toInt() or 0x08).toByte()
                    }
                }
                4 -> {
                    val xPoint = (src.width.toDouble() * 0.1).toInt()
                    val yPoint = (src.height.toDouble() * 0.72).toInt()

                    var count = 0
                    for (i in -1 until 2) {
                        for (j in -3 until 4) {
                            val pixel = src.getPixel(xPoint + i, yPoint + j)
                            val R = Color.red(pixel)
                            if (R < 255){count += 1}
                            src.setPixel(xPoint + i, yPoint + j, Color.argb(255, 255, 0, 0))
                        }
                    }

                    if (count >= 10){
                        seg = (seg.toInt() or 0x10).toByte()
                    }
                }
                5 -> {
                    val xPoint = (src.width.toDouble() * 0.1).toInt()
                    val yPoint = (src.height.toDouble() * 0.28).toInt()

                    var count = 0
                    for (i in -1 until 2) {
                        for (j in -3 until 4) {
                            val pixel = src.getPixel(xPoint + i, yPoint + j)
                            val R = Color.red(pixel)
                            if (R < 255){count += 1}
                            src.setPixel(xPoint + i, yPoint + j, Color.argb(255, 255, 0, 0))
                        }
                    }
                    if (count >= 10){
                        seg = (seg.toInt() or 0x20).toByte()
                    }
                }
                else -> {
                    val xPoint = (src.width.toDouble() * 0.5).toInt()
                    val yPoint = (src.height.toDouble() * 0.48).toInt()

                    var count = 0
                    for (i in -3 until 4) {
                        for (j in -1 until 2) {
                            val pixel = src.getPixel(xPoint + i, yPoint + j)
                            val R = Color.red(pixel)
                            if (R < 255){count += 1}
                            src.setPixel(xPoint + i, yPoint + j, Color.argb(255, 255, 0, 0))
                        }
                    }
                    if (count >= 10){
                        seg = (seg.toInt() or 0x40).toByte()
                    }
                }


            }
        }
    }
    return seg.toInt()
}

fun getDecimalNumber(position: Int, decimal: Int): Int{
    var multi = 1
    if (position == 1){multi = 10}
    else if (position == 2){multi = 100}
    when (decimal){
        6 -> return multi * 1
        91 -> return multi * 2
        79 -> return multi * 3
        102 -> return multi * 4
        109 -> return multi * 5
        125 -> return multi * 6
        39 -> return multi * 7
        7 -> return multi * 7
        127 -> return multi * 8
        111 -> return multi * 9
        else -> return 0
    }
}


fun Mat.cropImage(src: Bitmap, rectCrop: Rect){
    Utils.bitmapToMat(src, this)
    val cropped = Mat(this, rectCrop)
    Imgproc.cvtColor(cropped, this, Imgproc.COLOR_RGBA2RGB)
}


fun Mat.isActionable(src: Bitmap): Boolean{
    var total = 0
    for (y in 0 until  src.height ) {

        for (x in 0 until src.width ) {
            val pixel = src.getPixel(x, y)
            val R = Color.red(pixel)
            if (R < 255) {
                total += 1
            }
        }
    }
    val prom = total.toDouble() / (src.height * src.width).toDouble()
    return prom > 0.1 && prom < 0.8
}

fun Mat.inGray() = this.type() == CvType.CV_8U

fun detectRangeColor(srcImg: Mat, low: Double, high: Double): Mat {

    val hsvImage = Mat()
    val color_range = Mat()

    Imgproc.cvtColor(srcImg, hsvImage, Imgproc.COLOR_BGR2GRAY)

    Core.inRange(hsvImage, Scalar(low, low, low), Scalar (high, high, high), color_range)

    return color_range
}